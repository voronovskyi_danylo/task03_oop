package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class FurnitureDetergent extends Detergent {

    private double price;

    private List<FurnitureDetergent> furnitureDetergentList;

    public FurnitureDetergent() {
        furnitureDetergentList = new ArrayList<FurnitureDetergent>();
        furnitureDetergentList.add(new FurnitureDetergent("Pronto", "Furniture detergent", 66.40));
        furnitureDetergentList.add(new FurnitureDetergent("Mr Muscle", "Furniture detergent", 52.30));
        furnitureDetergentList.add(new FurnitureDetergent("Rainbow", "Furniture detergent", 66.40));
    }

    public FurnitureDetergent(String name, String type, double price) {
        super(name, type);
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<FurnitureDetergent> getFurnitureDetergentList() {
        for (int i = 0; i < furnitureDetergentList.size(); i++) {
            System.out.println(furnitureDetergentList.get(i));
        }
        return furnitureDetergentList;
    }

    public List<FurnitureDetergent> furnitureBucketOperation() {
        return furnitureDetergentList;
    }

    public void setFurnitureDetergentList(List<FurnitureDetergent> furnitureDetergentList) {
        this.furnitureDetergentList = furnitureDetergentList;
    }

    @Override
    public String toString() {
        return  super.toString() + price + "$ ||";
    }
}
